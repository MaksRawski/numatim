use numatim::Dict;
use std::fs;

pub fn setup(lang: &str) -> Dict {
    let dict_location = format!("dicts/{}.json", lang);
    let dict = fs::read_to_string(dict_location).unwrap();
    serde_json::from_str(&dict).unwrap()
}
