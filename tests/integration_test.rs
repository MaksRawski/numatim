use numatim;
mod common;

#[test]
fn test_en() {
    let dict = common::setup("en");
    let expected = String::from(
        "one hundred twenty three million four hundred fifty six thousand seven hundred eighty nine",
    );
    let actual = numatim::verbatim("123456789".to_string(), dict);
    assert_eq!(Ok(expected), actual);
}

#[test]
fn test_pl() {
    let dict = common::setup("pl");
    let expected = String::from(
        "sto dwadzieścia trzy miliony czterysta pięćdziesiąt sześć tysięcy siedemset osiemdziesiąt dziewięć"
        );
    let actual = numatim::verbatim("123456789".to_string(), dict);
    assert_eq!(Ok(expected), actual);
}
